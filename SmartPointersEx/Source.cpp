#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <algorithm>
#include <chrono>
#include "SimpleSingleton.h"

int Example1_SumOfSomeNumbers()
{
	const int numbers = 10;
	//std::unique_ptr<int[]> 
	auto a = std::make_shared<std::vector<int>>();
	//int *a = new int[numbers];
	for (int i = 0; i < numbers; i++)
	{
		a.get()->push_back(i * 2);
	
		//a.get() = i * 2;
		//a[i] = i * 2;
	}
	// do stuff, like multiplying all the elements by themselves
	for (int i = 0; i < numbers; i++)
	{
		//a.get()->push_back(a->at(i) * a->at(i)); same as below
		a->at(i) = (a->at(i) * a->at(i));

	}

	// return the sum of the elements
	int sum = 0;
	for (int i = 0; i < numbers; i++)
	{
		sum += a->at(i);
	}
//	delete a;
	return sum;

}

// ------------------------------------------------------------------------------

void Example2_MatricesAreFun()
{
	const int numberOfRows = 2;
	const int numberOfColumns = 3;
	//int **matrix = new int*[numberOfRows];
//	auto matrix = std::make_unique<std::unique_ptr<int[]>[]>(numberOfRows);
	auto matrix = std::make_shared<std::vector<std::shared_ptr<std::vector<int>>>>(numberOfRows);
	for (int i = 0; i < numberOfRows; i++)
	{
		//matrix[i] = std::make_unique<int[]>(numberOfColumns);
		matrix.get()->at(i) = std::make_shared<std::vector<int>>(numberOfColumns);
	}

	for (int i = 0; i < numberOfRows; i++)
		for (int j = 0; j < numberOfColumns; j++)
			//matrix[i][j] = i + j;       //matrix.get()[0].get()[2]
			(matrix->at(i))->at(j) = i + j;

	//for (int i = 0; i < numberOfRows; i++)
	//		delete[] matrix[i];//delete *(matrix + i)
	//	
	//delete[] matrix;
}

// ------------------------------------------------------------------------------

void Example3()
{
	auto a = std::make_unique<int[]>(100); //int a[100]; int** a = new int*[100];
	auto b = std::make_unique<int[]>(100);
	//auto c = std::make_unique<int>(100); // int *c = new int(100);

	//int *a = new int[100];
	//int *b = new int[100];

	// operations
	for (int i = 0; i < 100; i++)
		a[i] = i + 1;

	for (int i = 0; i < 100; i++)
		b[i] = i * i;

	// other operations
	//for(int i = 0; i < 100; i++)
	//b[i] = a[i];
	b.reset(a.release());

	// some more operations

	// like the good developers we are, we free both pointers
//	delete[] a;
//	delete[] b;
}

// ------------------------------------------------------------------------------

// GetTheIndexOfTheFirstLargerThanOrEqualToANumberNumberInTheFibonacciSequence
int Example4_WhatDoesThisDo(int number)
{
	const int sufficientNumberOfValues = 10;
	if (number == 0)
		return 0;
	else if (number == 1)
		return 1;

	//int *list = new int[sufficientNumberOfValues];
	auto list = std::make_unique<int[]>(sufficientNumberOfValues);
	int i = 0;
	for (; i < sufficientNumberOfValues; i++)
	{
		if (i == 0)
			list[i] = 0;
		else if (i == 1)
			list[i] = 1;
		if (i == 0 || i == 1)
			continue;
		list[i] = list[i - 1] + list[i - 2];
		if (number <= list[i])
		{
			//delete list;
			return i;
		}
	}
//	delete list;
	return i;
	
}

// ------------------------------------------------------------------------------

int Example5_Calculate(int* list, int numberOfValues)
{
	int sum = 0;
	for (int i = 0; i < numberOfValues; i++)
	{
		sum += list[i];
	}
	//delete[] list;
	return sum;
}

int Example5_SumOfFirstNNumbers(int n)
{
//	int *a = new int[n];
	auto a = std::make_unique<int[]>(n);
	for (int i = 0; i < n; i++)
	{
		a[i] = i;
	}
	int sum = Example5_Calculate(a.get(), n);
	//delete[] a;
	return 0;
}

// -----------------------Performance issues-------------------------------------

class GenericObject {
public:
	GenericObject(std::vector<double> values, int otherValues) : m_values(values), m_otherValue(otherValues)
	{

	}
	std::vector<double> m_values;
	int m_otherValue;
};

double PerformanceIssue_OperationV1(const GenericObject* genericObject)
{
	double sum = 0;
	for (const auto& value : genericObject->m_values)
		sum += value;
	sum -= genericObject->m_otherValue;
	return sum;
}

double PerformanceIssue_OperationV2(std::shared_ptr<GenericObject> genericObject)
{
	double sum = 0;
	for (const auto& value : genericObject->m_values)
		sum += value;
	sum -= genericObject->m_otherValue;
	return sum;
}

void PerformanceIssueV1()
{
	auto genericObject_v1 = std::make_unique<GenericObject>(std::vector<double>{ 0.0, 1.0, 3.2, 11.9, 83.11111, 342.01 }, 3);
	// Get starting timepoint 
	auto start = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < 20000000; i++)
		PerformanceIssue_OperationV1(genericObject_v1.get());
	// Get ending timepoint 
	auto stop = std::chrono::high_resolution_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

	std::cout << "Time taken by function Operation V1: " << duration.count() << " milliseconds. \n";
}

void PerformanceIssueV2()
{
	auto genericObject_v2 = std::make_shared<GenericObject>(std::vector<double>{ 0.0, 1.0, 3.2, 11.9, 83.11111, 342.01 }, 3);
	// Get starting timepoint 
	auto start = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < 20000000; i++)
		PerformanceIssue_OperationV2(genericObject_v2);
	// Get ending timepoint 
	auto stop = std::chrono::high_resolution_clock::now();

	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

	std::cout << "Time taken by function Operation V2: " << duration.count() << " milliseconds. \n";
}

// ------------------------------------------------------------------------------

class Person
{
public:
	Person(const std::string & aName) : mName(aName) {}
	std::string mName;
};

void printPeople(const std::vector<std::shared_ptr<Person>> v)
{
	for (const auto & el : v)
		std::cout << el->mName << "\n";
	std::cout << "\n";
}

void Example6_CreateAndPrintVectorOfPeople()
{
	std::vector<std::shared_ptr<Person>> people{ std::make_shared<Person>("Luiza"), std::make_shared<Person>("Radu"), std::make_shared<Person>("Iulia"), std::make_shared<Person>("George") };
	printPeople(people);
	people.clear();
}

// ------------------------------------------------------------------------------

class Pilot
{
public:
	Pilot(std::string name) :mName(name)
	{
		std::cout << "Pilot " << name << " created!\n";
	};

	~Pilot()
	{
		
		if (auto sharedFromWeak = mWingman.lock())
		{
			std::cout << "Pilot " << mName << " destroyed!\n";
				std::cout << "Bye Bye" << sharedFromWeak->mName << "\n";
		}
	};

	std::weak_ptr<Pilot> mWingman;
	std::string mName;
};

void Example7_CreatePilots()
{
	std::shared_ptr<Pilot> john = std::make_shared<Pilot>("John");
	std::shared_ptr<Pilot> dan = std::make_shared<Pilot>("Dan");

	john->mWingman = dan;
	dan->mWingman = john;
}

// ------------------------------------------------------------------------------

int main()
{

//	auto myLogger = Logger::getInstance();
//
//	try
//	{
//
////	myLogger->startLogging("salut.txt");
//	myLogger->log(Logger::Level::Info, "Ce mai faci?");
//
//	}
//	catch (const std::exception&  e)
//	{
//		std::cout << e.what() << '\n';
//	}
	// first example
	//std::cout << Example1_SumOfSomeNumbers() << '\n';

	// second example
	 //Example2_MatricesAreFun();

	// third example
	//Example3();

	// fourth example
	 //std::cout << Example4_WhatDoesThisDo(4) << '\n';

	// fifth example
	//std::cout << Example5_SumOfFirstNNumbers(11) << '\n';

	// sixth example
	// Example6_CreateAndPrintVectorOfPeople();

	// Performance problems
	// PerformanceIssueV1();
	// PerformanceIssueV2();

	// seventh example
	Example7_CreatePilots();

	/*int* x = new int;
	x[0] = 5;

	std::shared_ptr<int> MyPtr(x);
	std::cout << *MyPtr.get() << std::endl;

	int *y = MyPtr.release();

	std::cout << y << "\n";*/
	return 0;

}