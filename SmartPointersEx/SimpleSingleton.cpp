#include "SimpleSingleton.h"

#include <stdexcept>

void Logger::startLogging(const std::string & fileName)
{
	_logger.open(fileName);
	if (!_logger)
		throw std::runtime_error("Something happened.");
}

void Logger::log(Level level, const std::string & message)
{
	if(!_logger.is_open())
		throw std::runtime_error("Call startLogging() before this.");

	_logger << kLevelToString.at(level) << ": " << message << '\n';
}

void Logger::closeStream()
{
	_logger.close();
}

std::shared_ptr<Logger> Logger::getInstance()
{
	static  std::weak_ptr<Logger> instance;
	if (auto ptr = instance.lock())
	{
		return ptr;
	}

	std::shared_ptr<Logger> ptr(new Logger);
	instance = ptr;
	return ptr;
}