#pragma once


#include <memory>
#include <iostream>
#include <cstdint>
#include <unordered_map>
#include <string>
#include <fstream>

class Logger
{
public:
	enum class Level : std::uint8_t
	{
		Fatal,
		Error,
		Warning,
		Debug,
		Info
	};

	const std::unordered_map<Level, std::string> kLevelToString
	{
		{Level::Debug, "Debug"},
	{Level::Error, "Error"},
	{Level::Fatal, "Fatal"},
	{Level::Warning, "Warning"},
	{Level::Info, "Info"},
	};

public:
	void startLogging(const std::string& fileName);
	void log(Level level, const std::string& message);
	void closeStream();
	static std::shared_ptr<Logger> getInstance();
private:
	std::ofstream _logger;
	Logger() {}
};

